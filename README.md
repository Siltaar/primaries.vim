## Primaries.vim
A vim theme for programmers, inpired by good code from
[mango.vim](https://github.com/goatslacker/mango.vim), good colors from
[mustang.vim](https://github.com/croaker/mustang-vim), in good readability
from [monochrome.vim](https://github.com/fxn/vim-monochrome).

I thought monochrome.vim was a good idea, but a bit extremist, so I added a few
drops of color.

I made 4 declinations :

- summer (the default one)
- winter (cold pastel)
- automn (vivid cold colors)
- spring (vivid hot, unfinished business, I use automn)

## Installation

	$ git clone https://framagit.org/Siltaar/primaries.vim
	cp primaries.vim/colors/primaries.vim ~/.vim/colors/

Then, place in your `.vimrc` :
	color primaries.vim

## Preview
![primaries_v2 vim](https://framagit.org/Siltaar/primaries.vim/raw/master/primaries.vim.png)

The font in demonstration is [Inconsolata](http://www.levien.com/type/myfonts/inconsolata.html).

## Best viewed with Redshift

Best viewed with [Redshift](http://jonls.dk/redshift/) active with the following configuration:
```ini
[redshift]
temp-day=3500
temp-night=2500
```

## Support this work

Please consider supporting this work via :

- [Patreon](https://patreon.com/metapress)
- [Liberapay](https://liberapay.com/Meta-Press.es)
- Bitcoins : bc1qpkj7cu33e5y4jxjuml3e799jv6kxra729c4u03c9vyk85cz6379swqdzs3
- or Ethereums : 0x89f5f205141fc8eE6540e96BF2DCec20190c584
